<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-03-26 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-23 16:58:04
 */

namespace app\actstar\controller;

class AlipayTrade extends ActstarBase {

	function initialize() {
		parent::initialize();

		$this->signupDao = model('actstar_manage/Signup');
	}

	public function doAlipay() {
		$signupId = input('param.signupId', '', '', 'intval');
		if (!$signupId) {
			$this->wapError('报名记录ID有误');
		}

		//支付返回地址
		$returnUrl = "http://".$_SERVER['HTTP_HOST']."/actstar/mySignup/detail/signupId/".$signupId;
		$this->assign('returnUrl', $returnUrl);

		if ($this->isWeixin) {
			return $this->fetch('pay_wx_alipay');
		}

		//获取报名记录信息
		$signupInfo = $this->signupDao->getInfo($signupId);

		//验证
		if (!$signupInfo['total_fee']) {
			$this->wapError('订单金额非法');
		}

		//=========准备
		$totalFee = $signupInfo['total_fee'];
		$orderNo = $signupInfo['order_no'];
		$subject = '支付商城订单'.$totalFee.'元';

		$totalFee = number_format($totalFee, 2);
		// if($ordtotal_fee < 1) {
		//     //转账到支付宝账户，输入的是邮箱账户，最低金额不小于0.01元；输入的是手机号账户，最低金额不小于1元。
		//     $ordtotal_fee = 1;
		// }

/* *
 * 功能：支付宝手机网站支付接口(alipay.trade.wap.pay)接口调试入口页面
 * 版本：2.0
 * 修改日期：2016-11-01
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 请确保项目文件有可写权限，不然打印不了日志。
 */

header("Content-type: text/html; charset=utf-8");
require_once EXTEND_PATH.'alipayTrade/wappay/service/AlipayTradeService.php';
require_once EXTEND_PATH.'alipayTrade/wappay/buildermodel/AlipayTradeWapPayContentBuilder.php';
require EXTEND_PATH.'alipayTrade/config.php';

/**************************请求参数**************************/

//服务器异步通知页面路径
$config['notify_url'] = 'http://'.$_SERVER['HTTP_HOST'].'/actstar/alipayTrade/notifyUrl';

//页面跳转同步通知页面路径
$config['return_url'] = 'http://'.$_SERVER['HTTP_HOST'].'/actstar/alipayTrade/returnUrl';

//商户订单号，商户网站订单系统中唯一订单号，必填
$out_trade_no = $orderNo;

//订单名称，必填
$subject = $subject;

//付款金额，必填
$total_amount = $totalFee;

//商品描述，可空
$body = '';

//超时时间
$timeout_express="1m";

$payRequestBuilder = new \AlipayTradeWapPayContentBuilder();
$payRequestBuilder->setBody($body);
$payRequestBuilder->setSubject($subject);
$payRequestBuilder->setOutTradeNo($out_trade_no);
$payRequestBuilder->setTotalAmount($total_amount);
$payRequestBuilder->setTimeExpress($timeout_express);

$payResponse = new \AlipayTradeService($config);
$result=$payResponse->wapPay($payRequestBuilder,$config['return_url'],$config['notify_url']);

return ;

	}

	function notifyUrl(){

/* *
 * 功能：支付宝服务器异步通知页面
 * 版本：2.0
 * 修改日期：2016-11-01
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。

 *************************页面功能说明*************************
 * 创建该页面文件时，请留心该页面文件中无任何HTML代码及空格。
 * 该页面不能在本机电脑测试，请到服务器上做测试。请确保外部可以访问该页面。
 * 如果没有收到该页面返回的 success 信息，支付宝会在24小时内按一定的时间策略重发通知
 */
require_once EXTEND_PATH.'alipayTrade/wappay/service/AlipayTradeService.php';
require EXTEND_PATH.'alipayTrade/config.php';


$arr=$_POST;
$alipaySevice = new \AlipayTradeService($config);
$alipaySevice->writeLog(var_export($_POST,true));
$result = $alipaySevice->check($arr);

/* 实际验证过程建议商户添加以下校验。
1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
4、验证app_id是否为该商户本身。
*/
if($result) {//验证成功
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//请在这里加上商户的业务逻辑程序代


	//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——

	//获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表

	$out_trade_no   = $_POST['out_trade_no'];      //商户订单号
	$trade_no       = $_POST['trade_no'];          //支付宝交易号
	$trade_status   = $_POST['trade_status'];      //交易状态
	$total_fee      = $_POST['total_fee'];         //交易金额
	$notify_id      = $_POST['notify_id'];         //通知校验ID。
	$notify_time    = $_POST['notify_time'];       //通知的发送时间。
	$buyer_email    = $_POST['buyer_email'];       //买家支付宝帐号；

	$parameter = array(
		"out_trade_no"   => $out_trade_no,      //商户订单编号；
		"trade_no"       => $trade_no,          //支付宝交易号；
		"total_fee"      => $total_fee,         //交易金额；
		"trade_status"   => $trade_status,      //交易状态
		"notify_id"      => $notify_id,         //通知校验ID。
		"notify_time"    => $notify_time,       //通知的发送时间。
		"buyer_email"    => $buyer_email,       //买家支付宝帐号
	);

	if($_POST['trade_status'] == 'TRADE_FINISHED') {

		//判断该笔订单是否在商户网站中已经做过处理
			//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
			//请务必判断请求时的total_amount与通知时获取的total_fee为一致的
			//如果有做过处理，不执行商户的业务程序
		db('Aaaa')->insert(['space'=>'actstar', 'module'=>'alipay', 'flag'=>'notify1', 'content'=>pw_var_export($parameter)]);

		require_once(APP_PATH.'actstar/orderFunction.php');
		_orderHandle($parameter, 'alipay', 0); //进行订单处理，并传送从支付宝返回的参数

		//注意：
		//退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
	}
	else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
		//判断该笔订单是否在商户网站中已经做过处理
			//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
			//请务必判断请求时的total_amount与通知时获取的total_fee为一致的
			//如果有做过处理，不执行商户的业务程序
		//注意：
		//付款完成后，支付宝系统发送该交易状态通知
		db('Aaaa')->insert(['space'=>'actstar', 'module'=>'alipay', 'flag'=>'notify2', 'content'=>pw_var_export($parameter)]);

		require_once(APP_PATH.'actstar/orderFunction.php');
		_orderHandle($parameter, 'alipay', 0); //进行订单处理，并传送从支付宝返回的参数

	}
	//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

	echo "success";		//请不要修改或删除

}else {
	//验证失败
	echo "fail";	//请不要修改或删除

}

	}

	function returnUrl() {

/* *
 * 功能：支付宝页面跳转同步通知页面
 * 版本：2.0
 * 修改日期：2016-11-01
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。

 *************************页面功能说明*************************
 * 该页面可在本机电脑测试
 * 可放入HTML等美化页面的代码、商户业务逻辑程序代码
 */
require_once EXTEND_PATH.'alipayTrade/wappay/service/AlipayTradeService.php';
require EXTEND_PATH.'alipayTrade/config.php';

$arr=$_GET;
$alipaySevice = new \AlipayTradeService($config);
$result = $alipaySevice->check($arr);

//print_r($_GET);
//print_r($result);

/* 实际验证过程建议商户添加以下校验。
1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
4、验证app_id是否为该商户本身。
*/
if($result) {//验证成功
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//请在这里加上商户的业务逻辑程序代码

	//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
	//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表

	$out_trade_no   = $_GET['out_trade_no'];      //商户订单号
	$trade_no       = $_GET['trade_no'];          //支付宝交易号
	$trade_status   = $_GET['trade_status'];      //交易状态
	$total_fee      = $_GET['total_fee'];         //交易金额
	$notify_id      = $_GET['notify_id'];         //通知校验ID。
	$notify_time    = $_GET['notify_time'];       //通知的发送时间。
	$buyer_email    = $_GET['buyer_email'];       //买家支付宝帐号；

	$parameter = array(
		"out_trade_no"   => $out_trade_no,      //商户订单编号；
		"trade_no"       => $trade_no,          //支付宝交易号；
		"total_fee"      => $total_fee,         //交易金额；
		"trade_status"   => $trade_status,      //交易状态
		"notify_id"      => $notify_id,         //通知校验ID。
		"notify_time"    => $notify_time,       //通知的发送时间。
		"buyer_email"    => $buyer_email,       //买家支付宝帐号
	);
	//echo "验证成功<br />外部订单号：".$out_trade_no;

	//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

/*
array(
	'out_trade_no' => 'BR180408031331k00000006u0000000237',
	'trade_no' => '2018041021001004510539031106',
	'total_fee' => NULL,
	'trade_status' => NULL,
	'notify_id' => NULL,
	'notify_time' => NULL,
	'buyer_email' => NULL,
)
 */
	db('Aaaa')->insert(['space'=>'actstar', 'module'=>'alipay', 'flag'=>'returnUrl', 'content'=>pw_var_export($parameter)]);

	require_once(APP_PATH.'actstar/orderFunction.php');
	list($signupId) = _orderHandle($parameter, 'alipay', 0); //进行订单处理，并传送从支付宝返回的参数

	return $this->wapSuccess('支付成功', url('actstar/mySignup/detail/', ['signupId'=>$signupId]));

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
else {
	//验证失败
	echo "验证失败";
}

	}

}