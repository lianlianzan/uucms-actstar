<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-03-26 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 12:53:03
 */

namespace app\actstar\controller;

class Mine extends ActstarBase {

	function initialize() {
		parent::initialize();

		//底部菜单标示
		$this->assign('bottomFlag', 'mine');
	}

	public function index() {

		//设置seo
        $this->setSeoFrontNew('我的');

		return $this->fetch();
	}

}