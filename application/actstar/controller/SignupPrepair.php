<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-07-11 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 16:29:21
 */

namespace app\actstar\controller;

class SignupPrepair extends ActstarBase {

	function initialize() {
		parent::initialize();
		$this->activeDao = model('actstar_manage/Active');
		$this->activeMeetingDao = model('actstar_manage/ActiveMeeting');
		$this->signupDao = model('actstar_manage/Signup');
	}

	//报名确认的页面
	public function index() {
		$this->checkIsLoginWap(); //提示用户登陆状态

		$kid = input('param.initKid', '', '', 'intval');
		
		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($kid);
		//print_r($activeInfo);
		//验证数据
		if (!$this->activeDao->verifyData($activeInfo, $this->ts)) {
			$this->wapError($this->activeDao->getError());
		}
		//验证高级数据
		if (!$this->activeDao->verifySeniorData($activeInfo, $this->ts)) {
			$this->wapError($this->activeDao->getError());
		}
		
		$buyNum = input('param.buyNum', '', '', 'intval');

		$this->assign('kid', $kid);
		$this->assign('activeInfo', $activeInfo);
		
		//获取用户总次数
		$signupTotalNum = $this->signupDao->getUserCountByKidAndUid($kid, $this->moonuid);
		//=====限制用户参与总次数
		if ($activeInfo['limit_total_num']) {
			$tmpLimitNum = $activeInfo['limit_total_num'];
			if ($activeInfo['limit_total_share_num']) {
				if ($this->moondb['is_share']) {
					$tmpLimitNum += $activeInfo['limit_total_share_num'];
				}
			}
			if ($signupTotalNum >= $tmpLimitNum) {
				procUnLock('dzp', $this->moonuid); //物理锁
				if ($activeInfo['limit_total_tip']) { //自定义提示
					$tipmsg = str_replace(array("{limit_total_num}"), array($activeInfo['limit_total_num']), $activeInfo['limit_total_tip']);
					$this->wapError($tipmsg);
				} else {
					$this->wapError('您的抽奖次数已满，每人最多允许抽'.$activeInfo['limit_total_num'].'次');
				}
			}
		}
		//=====限制用户每日参与次数
		if ($activeInfo['limit_day_num']) {
			$recordTodayNum = $this->signupDao->getUserTdCountByKidAndUid($kid, $this->moonuid, $this->tdtime);
			$tmpLimitNum = $activeInfo['limit_day_num'];
			if ($activeInfo['limit_day_share_num']) {
				//获取用户分享数据
				$checkShareInfo = $this->shareDao->getUserTdCountByKidAndUid($kid, $this->moonuid, $this->tdtime);
				if ($checkShareInfo) {
					$tmpLimitNum += $activeInfo['limit_day_share_num'];
				}
			}
			if ($recordTodayNum >= $tmpLimitNum) {
				procUnLock('dzp', $this->moonuid); //物理锁
				if ($activeInfo['limit_day_tip']) { //自定义提示
					$tipmsg = str_replace(array("{record_today_num}", "{limit_day_num}", "{limit_day_share_num}"), array($recordTodayNum, $activeInfo['limit_day_num'], $activeInfo['limit_day_share_num']), $activeInfo['limit_day_tip']);
					$this->wapError($tipmsg);
				} else {
					if ($activeInfo['limit_day_share_num']) {
						$this->wapError('您今日已经参与'.$recordTodayNum.'次，每日只允许参与'.$activeInfo['limit_day_num'].'次（额外增加分享'.$activeInfo['limit_day_share_num'].'次）', '', ['mode'=>'alert']);
					} else {
						$this->wapError('您今日已经参与'.$recordTodayNum.'次，每日只允许参与'.$activeInfo['limit_day_num'].'次', '', $resultData);
					}
				}
			}
		}
		
		//开启场次的情况
		if ($activeInfo['is_meeting_open']) {
			$meid = input('param.initMeid', '', '', 'intval');
			if ($meid) {
				$meetingInfo = $this->activeMeetingDao->getInfo($meid);
				if ($meetingInfo['stock_num']) {
					$meetingRecordTotalNum = $this->signupDao->countSearch(['kid'=>$kid, 'meid'=>$meid]);
					if ($meetingRecordTotalNum > $meetingInfo['stock_num']) {
						$this->wapError('该名额不足无法报名');
					}
				}
				if ($meetingInfo['person_num']) {
					
				}
			}
		} else {
			$meetingInfo = array();
		}
		$this->assign('meid', $meid);
		$this->assign('meetingInfo', $meetingInfo);

		//设置seo
		$this->setSeoFrontNew('提交报名信息');
		
		return $this->fetch();
	}

}