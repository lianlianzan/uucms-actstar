<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-10-30 16:40:16
 * @Last Modified by:   zhaozilong
 * @Last Modified time: 2020-04-28 08:50:46
 */

namespace app\actstar\controller;

class ActiveDraw extends ActstarBase {

	function initialize() {
		parent::initialize();
		$this->activeDao = model('actstar_manage/Active');
		$this->signupDao = model('actstar_manage/Signup');
		$this->userDao = model('actstar_manage/User');
		$this->lotteryprizeDao = model('actstar_manage/Lotteryprize');
	}

	/*
	 * 抽奖地址线上：http://wx.ksmeishi.cn/actstar/activeDraw/index/kid/13.html
	 * 抽奖地址本地：http://www2.kszhuangxiu.com/actstar/activeDraw/index/kid/13.html
	 */
	public function index() {
		$kid = input('param.kid', '', '', 'intval');
		$this->assign('kid', $kid);
		
		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($kid);
		$lottery_progress = unserialize($activeInfo['lottery_progress']);
		$this->assign('activeInfo', $activeInfo);
		$this->assign('lottery_progress', $lottery_progress);
		
		//获取抽奖奖品列表
		list($lotteryprizeList) = $this->lotteryprizeDao->search(['status'=>1], '');
		$this->assign('lotteryprizeList', $lotteryprizeList);
		
		//获取记录列表
		$map = [
			'kid' 				=> ['kid', '=', $kid],
			'lotteryprize_id'	=> ['lotteryprize_id', '>', 0]
		];
		$map = array_values($map);
		list($signupList) = $this->signupDao->search($map, '');
		//print_r($signupList);
		$lotteryGroup = array();
		foreach ($signupList as $key => $value) {
			if ($value['lotteryprize_id'] > 0) {
				$lotteryGroup[$value['lotteryprize_id']][] = $value['id'];
			}
		}
		//print_r($lotteryGroup);
		$this->assign('signupList', $signupList);
		$this->assign('lotteryGroup', $lotteryGroup);
		
		//设置seo
		$this->setSeoOrigin($activeInfo['title'].'抽奖平台');
		
		return $this->fetch();
	}

	public function doDraw() {
		$kid = input('param.kid', '', '', 'intval');
		if (!$kid) {
			$this->error('活动ID非法');
		}
		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($kid);
		if ($activeInfo['is_lottery'] == 1) {
			$this->error('该活动已经抽奖，请勿重复抽');
		}
// 		$lottery_progress = unserialize($activeInfo['lottery_progress']);
// 		if ($lottery_progress[$type] == 1) {
// 			$this->error('该类型已经抽奖，请勿重复抽');
// 		}
		//获取抽奖奖品列表
		list($lotteryprizeList) = $this->lotteryprizeDao->search(['status'=>1], '');
		if (procLock('actstar_draw', $this->moonuid)) { //物理锁
			$totalNum = 0;
			foreach ($lotteryprizeList as $key => $value) {
				//获取报名列表
				$map = [];
				$map[] = ['kid', '=', $kid];
				$map[] = ['lotteryprize_id', '=', 0];
				list($signupList, , , , $cheatYesSignupIds, $cheatNoSignupIds) = $this->signupDao->search($map, '');
				//print_r($signupList);exit;
				//print_r($cheatYesSignupIds);exit;
				//奖品个数
				$total_num = $value['total_num'];
				if (count($signupList) < $total_num) {
					$this->error('报名人数小于奖品个数，无法抽奖');
				}
				if (count($cheatYesSignupIds) > $total_num) {
					$this->error('中奖人数设置有误，无法抽奖');
				}
				//先处理作弊的用户
				$this->signupDao->where([['id', 'IN', $cheatYesSignupIds]])->update(['lotteryprize_id'=>$value['id']]);
				$cheatNoNum = count($cheatNoSignupIds);
				$total_num = $total_num - $cheatNoNum;
				//echo $total_num;exit;
				$random_keys = array_rand($cheatNoSignupIds, $total_num);
				//print_r($random_keys);exit;
				//db('Aaaa')->insert(['space'=>'dzpDraw', 'module'=>'draw', 'flag'=>'random_keys', 'content'=>pw_var_export($random_keys1)]);
				$this->signupDao->where([['id', 'IN', $random_keys]])->update(['lotteryprize_id'=>$value['id']]);
				$totalNum += $value['total_num'];
			}
			$this->activeDao->baseUpdateData($kid, ['lottery_status'=>1]);
			procUnLock('actstar_draw', $this->moonuid); //物理锁
			$this->success('抽奖成功，共抽取'.$totalNum.'个', url('actstar/activeDraw/index', ['kid'=>$kid]));
		} else {
			procUnLock('actstar_draw', $this->moonuid); //物理锁
			$this->success('请重新操作');
		}
	}

	/**
	 * 清空抽奖信息线上：http://wx.ksmeishi.cn/actstar/activeDraw/doClear/kid/13.html
	 * 清空抽奖信息本地：http://www2.kszhuangxiu.com/actstar/activeDraw/doClear/kid/13.html
	 */
	public function doClear000() {
		$kid = input('param.kid', '', 'intval');

		$this->signupDao->where(['kid'=>$kid])->update(['lotteryprize_id'=>0]);

		$this->activeDao->baseUpdateData($kid, ['is_lottery'=>0]);

		echo '清空中奖信息成功';exit;
	}
	
}