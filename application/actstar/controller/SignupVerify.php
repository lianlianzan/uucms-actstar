<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-03-26 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-19 10:03:46
 */

namespace app\actstar\controller;

class SignupVerify extends ActstarBase {

	function initialize() {
		parent::initialize();

		$this->activeDao = model('actstar_manage/Active');
	}

	//报名活动进行验证
	public function doSignupVerify() {
		$this->checkIsLoginAjax(); //提示用户登陆状态

		$initKid = input('param.initKid', '', '', 'intval');
		if (!$initKid) {
			$this->error('活动ID不存在');
		}

		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($initPid);
		//验证数据
        if (!$this->activeDao->verifyData($activeInfo, $this->ts)) {
	        $this->error($this->activeDao->getError());
	    }
		//验证高级数据
		if (!$this->activeDao->verifySeniorData($activeInfo, $this->ts)) {
			$this->error($this->activeDao->getError());
		}

		//input数据
		$buyNum = input('param.buyNum', '', '', 'intval');

		//留言字段相关,验证
		if ($productExtendInfo['leavemess_fields']) {
			$messageData = input('param.messageData/a', '', '', 'pwEscape');
			$leavemess_fields_arr = unserialize($productExtendInfo['leavemess_fields']);
			$dataMessage = array();
			foreach ($leavemess_fields_arr as $key => $value) {
				if ($value['required']) { //必填
					if (!$messageData[$value['flag']]) {
						$this->error('商品留言开启,请填写'.$value['name']);
					}
				}
			}
		}

		$this->success();
	}

}