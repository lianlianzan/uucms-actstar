<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-03-26 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 12:53:03
 */

namespace app\actstar\controller;

class Index extends ActstarBase {

	function initialize() {
		parent::initialize();
		
		//底部菜单标示
		$this->assign('bottomFlag', 'index');
	}

	function index() {
		//获取导航菜单列表
		$navigationDao = model('actstar_manage/Navigation');
		list($navigationList) = $navigationDao->search(['status'=>1], '');
		$this->assign("navigationList", $navigationList);

		//缓存相关,生成缓存,获取图片轮播列表
		$sys_identify = 'actstar_slide';
		$cacheData = cache($cacheData);
		if (empty($retAnnounceInfo)) {
			$slideDao = model('actstar_manage/Slide');
			list($slideList) = $slideDao->search(['status'=>1], '');
			cache($sys_identify, $slideList, 3600);
		} else {
			$slideList = $cacheData;
		}
		//print_r($slideList);
		$this->assign("slideList", $slideList);

		//获取公告列表缓存
		$announceDao = model('actstar_manage/Announce');
		list($hotlineList) = $announceDao->search(['status'=>1], '');
		$this->assign("hotlineList", $hotlineList);
		
		//获取活动列表
		//$map = array(
		//	'status' => 1
		//);
		//$count = $this->activeDao->countSearch($map);
		//$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		//$pageShow = $Page->show();
		//list($activeList) = $this->activeDao->search($map, $Page->getLimit());
		////print_r($activeList);
		//$this->assign("activeList", $activeList);
		
		//获取文章列表
		//$map = array(
		//	'status' => 1
		//);
		//$count = $this->articleDao->countSearch($map);
		//$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		//$pageShow = $Page->show();
		//list($articleList) = $this->articleDao->search($map, $Page->getLimit());
		//$this->assign("articleList", $articleList);
		
		//获取自定义模板列表
		$wapcustomLogic = logic('actstar_manage/Wapcustom');
		list($wapcustomList) = $wapcustomLogic->getTemplate();
		//echo '自定义模板列表:';print_r($wapcustomList);
		$this->assign('wapcustomList', $wapcustomList);
		
		//分享相关
		$_share = array();
		$_share['title'] = $this->configInfo['share_title'];
		$_share['desc'] = $this->configInfo['share_desc'];
		$_share['link'] = '';
		$_share['imgUrl'] = $this->configInfo['share_icon'];
		$this->assign('_share', $_share);

		//设置seo
        $this->setSeoFrontNew('首页');
        
		return $this->fetch();
	}

	public function saveLocation() {
		$longitude = input('post.longitude', '', '', 'pwEscape');
		$latitude = input('post.latitude', '', '', 'pwEscape');
		$accuracy = input('post.accuracy', '', '', 'pwEscape');
		//=====更新用户信息
		if ($this->moonuid) {
			$data = array(
				'longitude'		=> $longitude,
				'latitude'		=> $latitude,
				'accuracy'		=> $accuracy,
				'location_time' => $this->ts
			);
			$this->userDao->baseUpdateData($this->moonuid, $data);
		}
	}
	
}