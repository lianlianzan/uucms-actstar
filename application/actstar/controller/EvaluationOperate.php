<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-03-26 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-19 14:46:41
 */

namespace app\actstar\controller;

class EvaluationOperate extends ActstarBase {

	function initialize() {
		parent::initialize();

		$this->evaluationDao = model('mobmall_manage/Evaluation');
		$this->evaluationPictureDao = model('mobmall_manage/EvaluationPicture');
		$this->orderDao = model('mobmall_manage/Order');
		$this->orderProductDao = model('mobmall_manage/OrderProduct');
		$this->productDao = model('mobmall_manage/Product');

		//设置seo
		$this->setSeoFrontNew('订单评价');
	}

	public function index() {
		$orderId = input('param.orderId', '', '', 'intval');

		//获取订单信息
		$orderInfo = $this->orderDao->getInfo($orderId);
		$this->assign('orderId', $orderId);
		$this->assign('orderInfo', $orderInfo);

		//获取订单商品列表
		list($orderProductList, , $pids) = $this->orderProductDao->getListByOrderId($orderId);
		//print_r($orderProductList);
		$this->assign("orderProductList", $orderProductList);

		//获取商品列表
		list($productList) = $this->productDao->getListByIds($pids);
		//print_r($productList);
		$this->assign("productList", $productList);

		return $this->fetch();
	}

	public function doSubmit() {
		$this->checkIsLoginAjax(); //提示用户登陆状态

		$orderId = input('post.orderId', '', '', 'intval');
		$content = input('post.content/a', '', '', 'pwEscape');
		$score = input('post.score/a', '', '', 'pwEscape');

		//获取订单信息
		$orderInfo = $this->orderDao->getInfo($orderId);

		//检测该订单是否已经评价
		if ($orderInfo['evaluation_status'] == 2) {
			$this->error('该订单已经评价，请勿重复评价');
		}

		//获取订单商品列表
		list($orderProductList, , $pids) = $this->orderProductDao->getListByOrderId($orderId);

		if (procLock('mobmall_evaluation', $this->moonuid)) { //加锁处理
			//循环订单商品列表
			foreach ($orderProductList as $key => $value) {
				$opid = $value['id'];
				//评价入库
				$data = array(
					'order_id'			=> $orderId,
					'pid'				=> $value['pid'],
					'uid'				=> $this->moonuid,
					'pwuid'				=> $this->moonpwuid,
					'username'			=> $this->moonname,
					'avatar'			=> $this->moonavatar,
					'create_time'		=> $this->ts,
					'content'			=> $content[$opid],
					'score'				=> $score[$opid],
				);
				if ($this->configInfo['evaluation_check']) { //开启评价审核
					$data['status'] = 1;
				} else {
					$data['status'] = 2;
				}
				//验证数据
				$validate = validate('mobmall_manage/Evaluation');
				if (!$validate->check($data)) {
					procUnLock('mobmall', $this->moonuid);
					$this->error($validate->getError());
				}
				//写数据库
				$result = $eid = $this->evaluationDao->baseAddData($data);
				if ($result === false) {
					$this->error('评价入库非法'.showDbError($this->evaluationDao));
				}
				//图片上传
				$pictureList = $this->h5uploadFilePatchForMulti('kszx/mobmall/evaluation', $opid);
				//图片入库
				if ($pictureList) {
					$pictureNum = count($pictureList);
					$data = [];
					foreach ($pictureList as $key => $value) {
						$data[] = array(
							'eid'    		=> $eid,
							'name'			=> $value['name'],
							'picurl'     	=> $value['attachurl'],
							'filetype'      => $value['type'],
							'filesize'      => $value['size'],
						);
					}
					$this->evaluationPictureDao->insertAll($data);

					//更新评价
					$data = array(
						'pic_num'    => $pictureNum,
					);
					$this->evaluationDao->baseUpdateData($eid, $data);
				}
				if ($this->configInfo['evaluation_check']) { //开启评价审核

				} else {
					//更新商品
					$this->productDao->where(array('id'=>$pid))->setInc('evaluation_num', 1);
				}
			}
			//更新订单
			$data = array(
				'evaluation_status'    => 2,
			);
			$this->orderDao->baseUpdateData($orderId, $data);

			procUnLock('mobmall', $this->moonuid);
			if ($result !== false) {
				$this->success('评价成功', url('mobmall/myOrder/detail', ['orderId'=>$orderId]));
			} else {
				$this->error('提交失败');
			}
		} else {
			$this->error('请重新操作');
		}
	}

	//h5upload批量上传
	public function h5uploadFilePatchForMulti($dir, $opid) {
		$fileName = input('post.fileName/a', '', '', 'pwEscape');
		$fileType = input('post.fileType/a', '', '', 'pwEscape');
		$fileSize = input('post.fileSize/a', '', '', 'pwEscape');
		$fileData = input('post.fileData/a', '', '', 'pwEscape');
		//print_r($fileType);exit;

		if (!$fileName) {
			return;
		}

		$attachs = array();
		$h5Upload = new \org\util\H5Upload();
		$h5Upload->saveFtpPath = $dir.'/Mon_'.date('ym') . '/';
		foreach ($fileName[$opid] as $key2 => $value2) {
			$h5Upload->fileName = $value2;
			$h5Upload->fileType = $fileType[$opid][$key2];
			$h5Upload->fileSize = $fileSize[$opid][$key2];
			$h5Upload->fileData = $fileData[$opid][$key2];
			$h5Upload->upload(); //开始上传
			if ($h5Upload->getError()) {
				$this->error($h5Upload->getError());
			}

			$uploadInfo = $h5Upload->getUploadInfo();

			$attachs[] = array(
				'name'		=> $uploadInfo['name'],
				'attachurl'	=> $uploadInfo['attachurl'],
				'type'		=> $uploadInfo['ext'],
				'size'		=> $uploadInfo['size'],
			);
		}

		//print_r($attachs);exit;
		return $attachs;
	}

}