<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2019-01-16 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 13:50:01
 */

namespace app\actstar\controller;

class CollectOperate extends ActstarBase {

	function initialize() {
		parent::initialize();

		$this->collectDao = model('actstar_manage/Collect');
	}

	public function doCollect() {
		$this->checkIsLoginAjax(); //提示用户登陆状态

		$kind = input('param.kind', '', '', 'intval');
		if (!$kind) {
			$this->error("收藏类型有误");
		}

		$zid = input('param.zid', '', '', 'intval');
		if (!$zid) {
			$this->error('非法ID');
		}

		$frompage = input('param.frompage', '', '', 'pwEscape');

		if ($kind == 1) {
			$productDao = model('actstar_manage/Product');
			$productInfo = $productDao->getInfo($zid);
			$msg1 = '商品已收藏，请勿重复收藏<br><a href="'.url('actstar/myCollect/index').'">查看我的收藏</a>';
			$msg2 = '商品收藏成功';
		}

		//检测用户是否收藏
		$collectInfo = $this->collectDao->getByUidAndKindAndZid($this->moonuid, $kid);
		if ($collectInfo) {
			$this->error($msg1);
		}

		$data = [
			'uid' => $this->moonuid,
			'kid' => $kid,
			'create_time' => $this->ts,
		];
		$this->collectDao->baseAddData($data);

		$this->success($msg2, '');
	}

	public function doCollectCancel() {
		$this->checkIsLoginAjax(); //提示用户登陆状态

		$kind = input('param.kind', '', '', 'intval');
		if (!$kind) {
			$this->error("收藏类型有误");
		}

		$zid = input('param.zid', '', '', 'intval');
		if (!$zid) {
			$this->error('非法ID');
		}

		$frompage = input('param.frompage', '', '', 'pwEscape');

		if ($kind == 1) {
			$msg1 = '商品已经取消收藏';
			$msg2 = '商品取消收藏成功';
		}

		//检测用户是否收藏
		$collectInfo = $this->collectDao->getByUidAndKindAndZid($this->moonuid, $kind, $zid);
		if (!$collectInfo) {
			$this->error($msg1);
		}

		$this->collectDao->delInfo($collectInfo['id']);

		$this->success($msg2, '');
	}

}