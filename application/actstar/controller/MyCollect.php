<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-03-26 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 13:50:01
 */

namespace app\actstar\controller;

class MyCollect extends ActstarBase {

	function initialize() {
		parent::initialize();

		$this->collectDao = model('actstar_manage/ActiveCollect');
		$this->activeDao = model('actstar_manage/Active');
	}

	public function index() {
		$this->checkIsLoginWap(); //提示用户登陆状态

		//获取列表
		$map = $parameter = array();

		$map['uid'] = $this->moonuid;

		$count = $this->collectDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($collectList, , $kids) = $this->collectDao->search($map, $Page->getLimit());
		$this->assign("count", $count);
		$this->assign('pageShow', $pageShow);
		$this->assign("collectList", $collectList);

		//获取活动列表
		list($activeList) = $this->activeDao->getListByIds($kids);
		$this->assign("activeList", $activeList);
		
		//设置seo
        $this->setSeoFrontNew('我的收藏');

		return $this->fetch();
	}

}