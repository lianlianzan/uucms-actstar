<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2017-09-27 16:40:00
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-08-04 18:22:09
 */

namespace app\actstar_manage\model;
use app\common\model\BaseNew;

class Signup extends BaseNew {
	// 设置数据表（不含前缀）
	protected $name = 'as_signup';

	// 设置当前模型的数据库连接
	protected $connection = 'db_kszhuangxiu_pieceapp_config';

	// 定义时间戳字段名
	protected $createTime = '';
	protected $updateTime = '';

	protected $_orderField = 'create_time';
	protected $_orderDesc = 'desc';

	//获取用户总次数
	public function checkMobileRepeat($kid, $mobile) {
		$map = array(
			'kid' 		=> $kid,
			'mobile' 	=> $mobile,
		);
		return $this->where($map)->count();
	}

	//获取用户总次数
	public function getUserCountByKidAndUid($kid, $uid) {
		$map = array(
			'kid' => $kid,
			'uid' => $uid,
		);
		return $this->where($map)->lock(true)->count();
	}

	//获取用户今日次数
	public function getUserTdCountByKidAndUid($kid, $uid, $tdtime) {
		$map = array(
			'kid' 			=> $kid,
			'uid' 			=> $uid,
			'create_time' 	=> array('gt', $tdtime),
		);
		return $this->where($map)->lock(true)->count();
	}

	public function countSearch($map) {
		$count = $this->where($map)->count();
		return $count;
	}

	public function search($map, $limit, $orderBy='') {
		if (!$orderBy) {
			$orderBy = array($this->_orderField=>$this->_orderDesc);
		}
		$data = $this->where($map)->order($orderBy)->limit($limit)->select();
		$data = $data->toArray(); //转换为数组
		//print_r($this->getLastSql());exit;
		return $this->parseSearch($data);
	}

	public function searchByGroupUid($map, $limit) {
		$data = $this->where($map)->group('uid')->having('count(uid)=1')->order('create_time desc')->limit($limit)->select();
		$data = $data->toArray(); //转换为数组
		//print_r($this->getLastSql());exit;
		return $this->parseSearch($data);
	}

	protected function parseSearch($data) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');
		$signup_status_config = config('moduleconfig.signup_status_config');
		$writeoff_status_config = config('extend.writeoff_status_config');

		$list = $recordIds = $kids = $uids = array();
		$cheatYesSignupIds = $cheatNoSignupIds = array();
		foreach ($data as $key => $value) {
			//通用解析
			//$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			//$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			//$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			//通用解析

			$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			$value['signup_status_show'] = isset($value['signup_status']) ? $signup_status_config[$value['signup_status']] : '';
			$value['writeoff_time_show'] = $value['writeoff_time'] ? date("Y-m-d H:i:s", $value['writeoff_time']) : '';
			$value['writeoff_status_show'] = isset($value['writeoff_status']) ? $writeoff_status_config[$value['writeoff_status']] : '';

 			$value['mobile_asterisk'] = substr($value['mobile'], 0, 3)."****".substr($value['mobile'], 8, 4);

			$list[$value['id']] = $value;
			$recordIds[$value['id']] = $value['id'];
			$kids[$value['kid']] = $value['kid'];
			$uids[$value['uid']] = $value['uid'];
			if ($value['cheat_prize_id']) {
				$cheatYesSignupIds[$value['id']] = $value['id'];
			} else {
				$cheatNoSignupIds[$value['id']] = $value;
			}
		}
		return array($list, $recordIds, $kids, $uids, $cheatYesSignupIds, $cheatNoSignupIds);
	}

	protected function parseInfo($info) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');
		$signup_status_config = config('moduleconfig.signup_status_config');
		$pay_status_config = config('moduleconfig.pay_status_config');
		$writeoff_status_config = config('extend.writeoff_status_config');

		//通用解析
		//$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';
		//$info['status_show'] = isset($info['status']) ? $isopen_config[$info['status']] : '';
		//$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';
		//通用解析

		$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';
		$info['signup_status_show'] = isset($info['signup_status']) ? $signup_status_config[$info['signup_status']] : '';
		$info['pay_status_show'] = isset($info['pay_status']) ? $pay_status_config[$info['pay_status']] : '';
		$info['writeoff_status_show'] = isset($info['writeoff_status']) ? $writeoff_status_config[$info['writeoff_status']] : '';

		return $info;
	}

	public function getInfoByOrderNo($orderNo) {
		$map = array(
			'order_no'	=> $orderNo,
		);
		$info = $this->where($map)->find();
		return $info ? $this->parseInfo($info->toArray()) : []; //转换为数组并解析
	}

}