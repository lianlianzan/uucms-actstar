<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2019-03-27 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 15:07:10
 */

namespace app\actstar_manage\model;
use app\common\model\BaseNew;

class User extends BaseNew {
	// 设置数据表（不含前缀）
	protected $name = 'as_user';

	// 设置当前模型的数据库连接
	protected $connection = 'db_kszhuangxiu_pieceapp_config';

	// 定义时间戳字段名
	protected $createTime = '';
	protected $updateTime = '';

	public function countSearch($map) {
		$count = $this->where($map)->count();
		return $count;
	}

	public function search($map, $limit) {
		$data = $this->where($map)->order('create_time desc')->limit($limit)->select();
		$data = $data->toArray(); //转换为数组
		//print_r($this->getLastSql());
		return $this->parseSearch($data);
	}

	protected function parseSearch($data) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		$list = array();
		foreach ($data as $key => $value) {
			//通用解析
			//$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			//$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			//$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			//通用解析

			$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			$value['last_login_time_show'] = $value['last_login_time'] ? date("Y-m-d H:i:s", $value['last_login_time']) : '';

			$list[$value['id']] = $value;
		}
		return array($list);
	}

	protected function parseInfo($info) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		//通用解析
		//$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';
		//$info['status_show'] = isset($info['status']) ? $isopen_config[$info['status']] : '';
		//$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';
		//通用解析

		return $info;
	}

	public function getInfoByUid($uid) {
		$map = array(
			'uid'		=> $uid,
		);
		$info = $this->where($map)->find();
		return $info ? $this->parseInfo($info->toArray()) : []; //转换为数组并解析
	}

	public function getInfoByPwuid($pwuid) {
		$map = array(
			'pwuid'		=> $pwuid,
		);
		$info = $this->where($map)->find();
		return $info ? $this->parseInfo($info->toArray()) : []; //转换为数组并解析
	}

	public function getInfoByOpenid($openid) {
		$map = array(
			'openid'	=> $openid,
		);
		$info = $this->where($map)->find();
		return $info ? $this->parseInfo($info->toArray()) : []; //转换为数组并解析
	}

}