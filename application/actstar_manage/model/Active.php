<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2018-08-18 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-23 15:08:01
 */

namespace app\actstar_manage\model;
use app\common\model\BaseNew;

class Active extends BaseNew {
	// 设置数据表（不含前缀）
	protected $name = 'as_active';

	// 设置当前模型的数据库连接
	protected $connection = 'db_kszhuangxiu_pieceapp_config';

	// 定义时间戳字段名
	protected $createTime = '';
	protected $updateTime = '';

	// 定义排序字段
	protected $_orderField = 'create_time';
	protected $_orderDesc = 'desc';

	public function verifyData($activeInfo, $nowTime) {
		if (!$activeInfo) {
			$this->error = '非法活动信息';
			return false;
		}
		if ($activeInfo['status'] == 0) {
			$this->error = '该活动已经关闭';
			return false;
		}
		return true;
	}

	public function verifySeniorData($activeInfo, $nowTime) {
		//检测活动库存
		if ($activeInfo['stock_num'] && $activeInfo['stock_num'] <= 0) {
			$this->error = '名额不足无法报名';
			return false;
		}
		if ($activeInfo['start_time'] && $nowTime < $activeInfo['start_time']) {
			if ($activeInfo['start_tip']) { //自定义提示
				$tipmsg = str_replace(array("{start_time}"), array($activeInfo['start_time_show']), $activeInfo['start_tip']);
				$this->error = $tipmsg;
				return false;
			} else {
				$this->error = '活动尚未开始，开始时间：'.$activeInfo['start_time_show'];
				return false;
			}
		}
		if ($activeInfo['end_time'] && $activeInfo['end_time'] < $nowTime) {
			if ($activeInfo['end_tip']) { //自定义提示
				$tipmsg = str_replace(array("{end_time}"), array($activeInfo['end_time_show']), $activeInfo['end_tip']);
				$this->error = $tipmsg;
				return false;
			} else {
				$this->error = '活动已经结束，结束时间：'.$activeInfo['end_time_show'];
				return false;
			}
		}
		return true;
	}

	public function updateHits($info) {
		$isHitCache = 1;
		$holdHit = 100;
		//点击加入缓存模式
		$randHits = rand(1, 3);
		if ($isHitCache) {
			$hitDao = model('actstar_manage/ActiveHit');
			$hitInfo = $hitDao->find($info['id']);
			if ($hitInfo) {
				if ($hitInfo['add_hits'] < $holdHit) {
					$hitDao->where(['id'=>$info['id']])->setInc('add_hits', $randHits);
				} else {
					$hitDao->where(['id'=>$info['id']])->update(['add_hits'=>0]);
					$newHits = $hitInfo['add_hits'] + $randHits;
					$this->where(['id'=>$info['id']])->setInc('hit_num', $newHits);
				}
			} else {
				$hitDao->insert(['id'=>$info['id'], 'add_hits'=>$randHits]);
			}
			$info['hit_num'] = $info['hit_num'] + $info['add_hits'] + $randHits;
		} else {
			$this->where(['id'=>$info['id']])->setInc('hit_num', $randHits);
			$info['hit_num'] = $info['hit_num'] + $randHits;
		}
		return $info;
	}

	public function countSearch($map) {
		return $this->where($map)->count();
	}

	public function search($map, $limit, $orderBy='') {
		if (!$orderBy) {
			$orderBy = array($this->_orderField=>$this->_orderDesc);
		}
		$data = $this->where($map)->order($orderBy)->limit($limit)->select();
		$data = $data->toArray(); //转换为数组
		return $this->parseSearch($data);
	}

	protected function parseSearch($data) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		$list = array();
		foreach ($data as $key => $value) {
			//通用解析
			//$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			//$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			//$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			//通用解析

			$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			$value['picurl2'] = $value['picurl2'] ? $ftpWeb.$value['picurl2'] : '';
			$value['picurl3'] = $value['picurl3'] ? $ftpWeb.$value['picurl3'] : '';
			$value['videourl'] = $value['videourl'] ? $ftpWeb.$value['videourl'] : '';
			$value['logo'] = $value['logo'] ? $ftpWeb.$value['logo'] : '';

			//组装链接
			$value['link'] = dealLinkTwo('active_detail', $value['id']);

			$list[$value['id']] = $value;
		}
		return array($list);
	}

	protected function parseInfo($info) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		//通用解析
		//$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';
		//$info['status_show'] = isset($info['status']) ? $isopen_config[$info['status']] : '';
		//$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';
		//通用解析

		$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';
		$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';
		$info['picurl2'] = $info['picurl2'] ? $ftpWeb.$info['picurl2'] : '';
		$info['picurl3'] = $info['picurl3'] ? $ftpWeb.$info['picurl3'] : '';
		$info['videourl'] = $info['videourl'] ? $ftpWeb.$info['videourl'] : '';
		$info['logo'] = $info['logo'] ? $ftpWeb.$info['logo'] : '';
		$info['bgimg'] = $info['bgimg'] ? $ftpWeb.$info['bgimg'] : '';
		$info['share_icon'] = $info['share_icon'] ? $ftpWeb.$info['share_icon'] : '';

		//留言字段相关
		$info['leavemess_fields_arr'] = $info['leavemess_fields'] ? unserialize($info['leavemess_fields']) : [];

		return $info;
	}

}