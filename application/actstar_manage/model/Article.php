<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2018-08-18 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-23 15:08:01
 */

namespace app\actstar_manage\model;
use app\common\model\BaseNew;

class Article extends BaseNew {
	// 设置数据表（不含前缀）
	protected $name = 'as_article';

	// 设置当前模型的数据库连接
	protected $connection = 'db_kszhuangxiu_pieceapp_config';

	// 定义时间戳字段名
	protected $createTime = '';
	protected $updateTime = '';

	// 定义排序字段
	protected $_orderField = 'create_time';
	protected $_orderDesc = 'desc';

	public function updateHits($info) {
		$rand = rand(1, 3);
		$this->where(['id'=>$info['id']])->setInc('hit_num', $rand);
	}

	public function countSearch($map) {
		return $this->where($map)->count();
	}

	public function search($map, $limit, $orderBy='') {
		if (!$orderBy) {
			$orderBy = array($this->_orderField=>$this->_orderDesc);
		}
		$data = $this->where($map)->order($orderBy)->limit($limit)->select();
		$data = $data->toArray(); //转换为数组
		return $this->parseSearch($data);
	}

	protected function parseSearch($data) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		$list = array();
		foreach ($data as $key => $value) {
			//通用解析
			//$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			//$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			//$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			//通用解析

			$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			$value['picurl2'] = $value['picurl2'] ? $ftpWeb.$value['picurl2'] : '';
			$value['picurl3'] = $value['picurl3'] ? $ftpWeb.$value['picurl3'] : '';
			$value['videourl'] = $value['videourl'] ? $ftpWeb.$value['videourl'] : '';

			//组装链接
			$value['link'] = dealLinkTwo('article_detail', $value['id']);

			$list[$value['id']] = $value;
		}
		return array($list);
	}

	protected function parseInfo($info) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		//通用解析
		//$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';
		//$info['status_show'] = isset($info['status']) ? $isopen_config[$info['status']] : '';
		//$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';
		//通用解析

		$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';
		$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';
		$info['picurl2'] = $info['picurl2'] ? $ftpWeb.$info['picurl2'] : '';
		$info['picurl3'] = $info['picurl3'] ? $ftpWeb.$info['picurl3'] : '';
		$info['videourl'] = $info['videourl'] ? $ftpWeb.$info['videourl'] : '';

		return $info;
	}

}