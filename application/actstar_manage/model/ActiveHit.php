<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-04-13 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-24 16:24:55
 */

namespace app\actstar_manage\model;
use app\common\model\BaseNew;

class ActiveHit extends BaseNew {
	// 设置数据表（不含前缀）
	protected $name = 'as_active_hit';

	// 设置当前模型的数据库连接
	protected $connection = 'db_kszhuangxiu_pieceapp_config';

	// 定义时间戳字段名
	protected $createTime = '';
	protected $updateTime = '';

	protected function parseSearch($data) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');
		$product_status_config = config('product_status_config');
		$product_type_config = config('product_type_config');

		$list = $ids = array();
		foreach ($data as $key => $value) {
			//通用解析
			//$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			//$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			//$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			//通用解析

			$list[$value['id']] = $value;
			$ids[$value['id']] = $value['id'];
		}
		return array($list, $ids);
	}

	protected function parseInfo($info) {
		//通用解析
		//$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';
		//$info['status_show'] = isset($info['status']) ? $isopen_config[$info['status']] : '';
		//$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';
		//通用解析

		return $info;
	}

}