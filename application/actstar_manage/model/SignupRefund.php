<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-07-11 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2020-12-26 15:53:57
 */

namespace app\actstar_manage\model;
use app\common\model\BaseNew;

class RecordRefund extends BaseNew {
	// 设置数据表（不含前缀）
	protected $name = 'as_signup_refund';

	// 设置当前模型的数据库连接
	protected $connection = 'db_kszhuangxiu_pieceapp_config';

	// 定义时间戳字段名
	protected $createTime = '';
	protected $updateTime = '';

	public function countSearch($map) {
		return $this->where($map)->count();
	}

	public function search($map, $limit) {
		$data = $this->where($map)->order('create_time desc')->limit($limit)->select();
		$data = $data->toArray(); //转换为数组
		//print_r($this->getLastSql());exit;
		return $this->parseSearch($data);
	}

	protected function parseSearch($data) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		$list = $ids = $uids = array();
		foreach ($data as $key => $value) {
			//通用解析
			//$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			//$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			//$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			//通用解析

			$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';

			$list[$value['id']] = $value;
			$ids[$value['id']] = $value['id'];
			$uids[$value['uid']] = $value['uid'];
		}
		return array($list, $ids, $uids);
	}

	protected function parseInfo($info) {
		$ftpWeb = config('app.ftp_web');

		//通用解析
		//$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';
		//$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';
		//$info['status_show'] = isset($info['status']) ? $isopen_config[$info['status']] : '';
		//通用解析

		$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';

		return $info;
	}

}