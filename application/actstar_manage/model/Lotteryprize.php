<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2017-09-27 16:40:00
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-02-01 13:31:02
 */

namespace app\actstar_manage\model;
use app\common\model\BaseNew;

class Lotteryprize extends BaseNew {
	// 设置数据表（不含前缀）
	protected $name = 'as_lotteryprize';

	// 设置当前模型的数据库连接
	protected $connection = 'db_kszhuangxiu_pieceapp_config';

	// 定义时间戳字段名
	protected $createTime = '';
	protected $updateTime = '';

	protected $_orderField = 'vieworder';
	protected $_orderDesc = 'desc';

	public function createData($data) {
		if (!$data['grade_name']) {
			$this->showError('奖项等级名称不能为空');
			return false;
		}
		if (!$data['name']) {
			$this->showError('奖品名称不能为空');
			return false;
		}
		if (!$data['prize_type']) {
			$this->showError('请选择奖品类型');
			return false;
		}
		return true;
	}

	public function getListByKid($kid) {
		$map = array(
			'kid'		=> $kid,
			'status'	=> 1,
		);
		$data = $this->where($map)->order('vieworder desc')->select();
		$data = $data->toArray(); //转换为数组
		//print($this->getLastSql());
		return $this->parseSearch($data);
	}

	//好像用不到了
	// public function getAvailableListByKid($kid) {
	// 	$map = array(
	// 		'kid'		=> $kid,
	// 		'status'	=> 1,
	// 		'stock'		=> array('gt', 0),
	// 	);
	// 	$data = $this->where($map)->order('vieworder desc')->select();
	// 	$data = $data->toArray(); //转换为数组
	// 	//print($this->getLastSql());
	// 	return $this->parseSearch($data);
	// }

	public function countSearch($map) {
		$count = $this->where($map)->count();
		return $count;
	}

	public function search($map, $limit) {
		$data = $this->where($map)->order('vieworder desc')->limit($limit)->select();
		$data = $data->toArray(); //转换为数组
		//print_r($this->getLastSql());
		return $this->parseSearch($data);
	}

	protected function parseSearch($data) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		$list = $probability = $yesPrizeList = $noPrizeList = $pids = array();
		$totalGoal = 0;
		foreach ($data as $key => $value) {
			//通用解析
			//$value['create_time_show'] = $value['create_time'] ? date("Y-m-d H:i:s", $value['create_time']) : '';
			//$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			//$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			//通用解析

			$value['status_show'] = isset($value['status']) ? $isopen_config[$value['status']] : '';
			$value['picurl'] = $value['picurl'] ? $ftpWeb.$value['picurl'] : '';
			$value['is_prize_show'] = $value['is_prize'] ? '是' : '否';

			$list[$value['id']] = $value;
			if ($value['status'] == 1 && $value['goal']) {
				if ($value['stock_num'] > 0) { //存在库存的情况
					if ($value['limit_stock_day_num']) { //限制每日个数的情况
						if ($value['today_stock_num'] > 0) {
							$probability[$value['id']] = $value['goal'];
							$totalGoal += (int) $value['goal'];
						}
					} else {
						$probability[$value['id']] = $value['goal'];
						$totalGoal += (int) $value['goal'];
					}
				}
				if ($value['is_prize']) {
					$yesPrizeList[$value['id']] = $value;
				} else {
					$noPrizeList[$value['id']] = $value;
				}
			}
			$pids[] = $value['id'];
		}
		return array($list, $probability, $yesPrizeList, $noPrizeList, $totalGoal, $pids);
	}

	protected function parseInfo($info) {
		$ftpWeb = config('app.ftp_web');
		$isopen_config = config('extend.isopen_config');

		//通用解析
		//$info['create_time_show'] = $info['create_time'] ? date("Y-m-d H:i:s", $info['create_time']) : '';
		//$info['status_show'] = isset($info['status']) ? $isopen_config[$info['status']] : '';
		//$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';
		//通用解析

		$info['picurl'] = $info['picurl'] ? $ftpWeb.$info['picurl'] : '';

		return $info;
	}

}