<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2018-08-18 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-02-25 16:42:47
 * 事件逻辑
 */

namespace app\actstar_manage\logic;
use app\common\logic\Base as BaseLogic;

class Wapcustom extends BaseLogic {

	/**
	 * 获取模板列表
	 */
	public function getTemplate() {
		$wapcustomPieceDao = model('actstar_manage/WapcustomPiece');
		$wapcustomInfopushDao = model('actstar_manage/WapcustomInfopush');
		//获取模板列表
		$map = [
			'status'	=> 1
		];
		list($pieceList, $pieceIds) = $wapcustomPieceDao->search($map, '');
		//print_r($pieceList);
		$wapcustomList = [];
		foreach ($pieceList as $key => $value) {
			$tmpArr = array();
			$tmpArr['name'] = $value['name'];
			$tmpArr['kind_id'] = $value['kind_id'];
			$tmpArr['is_show_hd'] = $value['is_show_hd'];
			if ($value['more_name']) {
				$tmpArr['more_name'] = $value['more_name'];
				$tmpArr['more_page_flag'] = $value['more_page_flag'];
				$tmpArr['more_page_data'] = $value['more_page_data'];
				$tmpArr['more_page_extra'] = $value['more_page_extra'];
			}
			if ($value['ad_picurl']) {
				$tmpArr['ad_picurl'] = $value['ad_picurl'];
				$tmpArr['ad_page_flag'] = $value['ad_page_flag'];
				$tmpArr['ad_page_data'] = $value['ad_page_data'];
			}
			$tmpArr['data_source'] = $value['data_source'];
			$tmpArr['data_sort'] = $value['data_sort'];
			$tmpArr['show_style'] = $value['show_style'];
			if ($value['kind_id'] == 2) { //模块类别
				$sourceList = array();
				$manualNum = 0;
				$manualKids = $manualArids = array();
				if ($value['infopush_first'] || $value['push_mode'] == 2) { //获取手动数据
					$map = [
						'piece_id' 	=> $value['id'],
						'status'	=> 1,
					];
					if ($value['num']) {
						$limit = 'limit 0,'.$value['num'];
					} else {
						$limit = 'limit 0,4';
					}
					list($infopushList, $pageFlags) = $wapcustomInfopushDao->search($map, $limit);
					//print_r($infopushList);
					$manualNum = count($infopushList);
					//获取活动列表
					if ($pageFlags['active_detail']) {
						$activeDao = model('actstar_manage/Active');
						list($activeList) = $activeDao->getListByIds($pageFlags['active_detail']);
						//echo '活动列表:';print_r($activeList);exit;
					}
					//获取文章列表
					if ($pageFlags['article_detail']) {
						$articleDao = model('actstar_manage/Article');
						list($articleList) = $articleDao->getListByIds($pageFlags['article_detail']);
						//echo '文章列表:';print_r($articleList);exit;
					}
					foreach ($infopushList as $key2 => $value2) {
						$tmp = array();
						$tmp['page_flag'] = $value2['page_flag'];
						$tmp['page_data'] = $value2['page_data'];
						$tmp['link'] = dealLinkTwo($tmp['page_flag'], $tmp['page_data']); //组装链接
						if ($value2['page_flag'] == 'active_detail') { //解析活动
							$productInfo = $productList[$value2['page_data']];
							$tmp['title'] = $value2['title'];
							$tmp['picurl'] = $value2['picurl'] ? $value2['picurl'] : $productInfo['picurl'];
							$tmp['real_price'] = $productInfo['real_price'];
							$tmp['origin_price'] = $productInfo['origin_price'];
							$tmp['sale_num'] = $productInfo['sale_num'];
							$sourceList[] = $tmp;
							$manualKids[] = $value2['page_data'];
						} else if ($value2['page_flag'] == 'article_detail') { //解析文章
							$shopInfo = $shopList[$value2['page_data']];
							$tmp['title'] = $shopInfo['title'];
							$tmp['picurl'] = $value2['picurl'] ? $value2['picurl'] : $productInfo['picurl'];
							$tmp['real_price'] = $shopInfo['real_price'];
							$tmp['origin_price'] = $shopInfo['origin_price'];
							$tmp['sale_num'] = $shopInfo['sale_num'];
							$sourceList[] = $tmp;
							$manualArids[] = $value2['page_data'];
						}
					}
					//echo '获取手动数据:';print_r($sourceList);exit;
				}
				$leaveNum = $value['num'] - $manualNum;
				if ($value['push_mode'] == 1 && $leaveNum) { //获取自动数据
					if ($value['infopush_first']) { //故意多调几个数据
						$limit = 'limit 0,'.$value['num'];
					} else {
						$limit = 'limit 0,'.$leaveNum;
					}
					if ($value['data_source'] == 'active') { //自动调取数据,活动
						$activeDao = model('actstar_manage/Active');
						$map = array();
						$map['status'] = 1;
						if ($value['data_category']) {
							$map['cid'] = $value['data_category'];
						}
						//排序类型
						$orderBy = '';
						if ($value['data_sort'] == 1) {
							$orderBy = 'create_time desc';
						} else if ($value['data_sort'] == 2) {
							$orderBy = 'vieworder desc';
						} else if ($value['data_sort'] == 3) {
							$orderBy = 'hit_num desc';
						} else if ($value['data_sort'] == 4) {
							$orderBy = 'signup_num desc';
						}
						list($activeList) = $activeDao->search($map, $limit, $orderBy);
						if ($value['id'] == 8) {
							//print_r($activeList);
						}
						$automaticSourceList = array();
						foreach ($activeList as $key2 => $value2) {
							if (in_array($value2['id'], $manualKids)) {
								continue;
							}
							$tmp = array();
							$tmp['page_flag'] = 'active_detail';
							$tmp['page_data'] = $value2['id'];
							$tmp['link'] = dealLinkTwo($tmp['page_flag'], $tmp['page_data']); //组装链接
							$tmp['title'] = $value2['title'];
							$tmp['picurl'] = $value2['picurl'];
							$tmp['create_time'] = $value2['create_time'];
							$tmp['exhibition'] = $value2['exhibition'];
							$tmp['real_price'] = $value2['real_price'];
							$tmp['origin_price'] = $value2['origin_price'];
							$tmp['hit_num'] = $value2['hit_num'];
							$automaticSourceList[] = $tmp;
						}
					} else if ($value['data_source'] == 'article') { //自动调取数据,文章
						$articleDao = model('actstar_manage/Article');
						$map = array();
						$map['status'] = 1;
						if ($value['data_category']) {
							$map['cid'] = $value['data_category'];
						}
						//排序类型
						$orderBy = '';
						if ($value['data_sort'] == 1) {
							$orderBy = 'create_time desc';
						} else if ($value['data_sort'] == 2) {
							$orderBy = 'vieworder desc';
						} else if ($value['data_sort'] == 3) {
							$orderBy = 'hit_num desc';
						}
						list($articleList) = $articleDao->search($map, $limit, $orderBy);
						//print_r($articleList);
						$automaticSourceList = array();
						foreach ($articleList as $key2 => $value2) {
							if (in_array($value2['id'], $manualArids)) {
								continue;
							}
							$tmp = array();
							$tmp['page_flag'] = 'article_detail';
							$tmp['page_data'] = $value2['id'];
							$tmp['link'] = dealLinkTwo($tmp['page_flag'], $tmp['page_data']); //组装链接
							$tmp['title'] = $value2['title'];
							$tmp['exhibition'] = $value2['exhibition'];
							$tmp['show_type'] = 1;
							$tmp['picurl'] = $value2['picurl'];
							$tmp['create_time'] = $value2['create_time'];
							if ($value2['picurl2']) {
								$tmp['show_type'] = 2;
								$tmp['picurl2'] = $value2['picurl2'];
								$tmp['picurl3'] = $value2['picurl3'];
							}
							if ($value2['videourl']) {
								$tmp['show_type'] = 3;
								$tmp['videourl'] = $value2['videourl'];
							}
							$tmp['cate_name'] = $value2['cate_name'];
							$tmp['catename'] = $value2['cate_name']; //todo delete
							$tmp['autotime'] = getAutoTime($value2['create_time']);
							$automaticSourceList[] = $tmp;
						}
					}
					$automaticSourceList && $sourceList = array_merge($sourceList, $automaticSourceList);
				}
				$tmpArr['sourceList'] = $sourceList;
			}
			$wapcustomList[] = $tmpArr;
		}
		return array($wapcustomList);
	}

}