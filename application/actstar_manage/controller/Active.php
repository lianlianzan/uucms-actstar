<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2017-09-27 16:40:00
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-19 15:47:56
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class Active extends ManageBase {

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '活动列表', '', 'active');

		$this->activeDao = model('Active');
		$this->activePictureDao = model('ActivePicture');
	}

	public function index() {
		//获取列表
		list($map, $parameter) = $this->getMap();
		$count = $this->activeDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($list) = $this->activeDao->search($map, $Page->getLimit());
		$this->assign('count', $count);
		$this->assign('pageShow', $pageShow);
		$this->assign('list', $list);

		return $this->fetch();
	}

	private function getMap() {
		$map = $parameter = array();
		
		$kid = input('param.kid', '', '', 'intval');
		if ($kid) {
			$map[] = ['kid', '=', $kid];
			$parameter['kid'] = $kid;
		}
		$this->assign('kid', $kid);
		
		$title = input('param.title', '', 'pwEscape');
		if ($title) {
			$map[] = ['title', 'like', '%'.$title.'%'];
			$parameter['title'] = $title;
		}
		$this->assign('title', $title);
		
		$content = input('param.content', '', 'pwEscape');
		if ($title) {
			$map[] = ['content', 'like', '%'.$content.'%'];
			$parameter['content'] = $content;
		}
		$this->assign('content', $content);
		
		$this->assign('parameter', $parameter);
		return array($map, $parameter);
	}
	
	public function getDemoUrl() {
		$id = input('param.id', '', '', 'intval');
		$this->assign('id', $id);

		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($id);
		$this->assign('activeInfo', $activeInfo);

		//获取微信公众号信息
		$weixinAccountDao = model('weixin_manage/WeixinAccount');
		$weixinAccountInfo = $weixinAccountDao->getInfo(config('system.authorize_wid'));
		$this->assign('authorize_domain', $weixinAccountInfo['authorize_domain']);

		//前台地址展示
		$prodUrl = 'http://'.$weixinAccountInfo['authorize_domain'].'/actstar/active/detail/kid/'.$id;
		$httpsUrl = 'https://'.$weixinAccountInfo['authorize_domain'].'/actstar/active/detail/kid/'.$id;
		$devUrl = config('system.site_host').'/actstar/active/detail/kid/'.$id;
		$this->assign('prodUrl', $prodUrl);
		$this->assign('httpsUrl', $httpsUrl);
		$this->assign('devUrl', $devUrl);

		echo $this->fetch()->getContent();
	}

	public function add() {
		$id = input('param.id', '', '', 'intval');

		if ($id) {
			$info = $this->activeDao->getInfo($id);
			//获取活动图片列表
			list($pictureList) = $this->activePictureDao->getListByKid($id);
		} else {
			$info = array(
				'status'        => 1,
			);
			//获取已传但未利用的附件
			list($pictureList) = $this->activePictureDao->getUnUseAttach();
		}
		//print_r($info);exit;
		$this->assign('id', $id);
		$this->assign('kid', $id);
		$this->assign('info', $info);
		$this->assign('pictureList', $pictureList);

		//获取微信公众号列表
		$weixinAccountDao = model('weixin_manage/WeixinAccount');
		list($wxaccountList) = $weixinAccountDao->getListByUid($this->adminuid);
		$this->assign('wxaccountList', $wxaccountList);

		//留言字段相关
		$this->assign('leavemess_option_config', config('moduleconfig.leavemess_option_config'));
		$this->assign('leavemess_type_config', config('moduleconfig.leavemess_type_config'));

		return $this->fetch();
	}

	public function doAdd() {
		$id = input('post.id', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');

		//处理编辑器内容
		$data['content'] = ueditorContent($data['content']);

		if ($data['end_time'] < $data['start_time']) {
			$this->error('结束时间不能小于开始时间');
		}

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/active', 'picurl', $info['picurl']);
		$uploadInfo && $data['picurl'] = $uploadInfo['saveFullPath'];

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/article', 'picurl2', $info['picurl2']);
		$uploadInfo && $data['picurl2'] = $uploadInfo['saveFullPath'];

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/article', 'picurl3', $info['picurl3']);
		$uploadInfo && $data['picurl3'] = $uploadInfo['saveFullPath'];

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/active', 'logo', $info['logo']);
		$uploadInfo && $data['logo'] = $uploadInfo['saveFullPath'];

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/active', 'bgimg', $info['bgimg']);
		$uploadInfo && $data['bgimg'] = $uploadInfo['saveFullPath'];

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/active', 'share_icon', $info['share_icon']);
		$uploadInfo && $data['share_icon'] = $uploadInfo['saveFullPath'];

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/active', 'poster_img', $info['poster_img']);
		$uploadInfo && $data['poster_img'] = $uploadInfo['saveFullPath'];

		$data['start_time'] = strtotime($data['start_time']);
		$data['end_time'] = strtotime($data['end_time']);

		//留言字段相关
		$leavemess_option_config = config('moduleconfig.leavemess_option_config');
		$leavemess_type_config = config('moduleconfig.leavemess_type_config');
		$leavemessFieldsCheckbox = input('post.leavemessFieldsCheckbox/a', '', '', 'pwEscape');
		$leavemessFields = input('post.leavemessFields/a', '', '', 'pwEscape');
		if ($leavemessFieldsCheckbox) {
			$fieldsTmp = [];
			foreach ($leavemessFieldsCheckbox as $key => $value) {
				if ($value == 1) {
					$tmp = [];
					$tmp['key'] = $key;
					$tmp['issys'] = $leavemess_option_config[$key]['issys'];
					$tmp['flag'] = $leavemess_option_config[$key]['flag'];
					$tmp['vieworder'] = $leavemessFields[$key]['vieworder'];
					$tmp['name'] = $leavemessFields[$key]['name'] ? $leavemessFields[$key]['name'] : $leavemess_option_config[$key]['name'];
					$tmp['desc'] = $leavemessFields[$key]['desc'];
					$tmp['is_require'] = $leavemessFields[$key]['is_require'];
					$tmp['config'] = $leavemessFields[$key]['config'];
					//非系统字段
					if (!$message_option_config[$key]['issys']) {
						$tmp['type'] = $leavemessFields[$key]['type'];
					}
					$fieldsTmp[$key] = $tmp;
				}
			}
			usort($fieldsTmp, "vieworderSort"); //排序
			$fieldsShow = [];
			foreach ($fieldsTmp as $key => $value) {
				$fieldsShow[$value['key']] = $value;
			}
			$fieldsShow = serialize($fieldsShow);
			$data['leavemess_fields'] = $fieldsShow;
		} else {
			$data['leavemess_fields'] = '';
		}

		//验证数据
		$validate = validate('Active');
		if (!$validate->check($data)) {
			$this->error($validate->getError());
		}

		if ($id) {
			$result = $this->activeDao->baseUpdateData($id, $data);
			if ($result === false) {
				$this->error('保存失败'.showDbError($this->activeDao));
			}
		} else {
			$data['create_time'] = $this->ts;
			$result = $id = $this->activeDao->baseAddData($data);
			if ($result === false) {
				$this->error('添加失败'.showDbError($this->activeDao));
			}
		}

		//==========更新活动信息
		$sqlData = [];
		//==========处理图片
		$flashatt = input('post.flashatt/a', '', 'pwEscape');
		$picNum = $flashatt ? count($flashatt) : 0;
		if ($picNum > 0 && $id) {
			$sqlDataTmp = $this->doPictures($id, $flashatt);
			$sqlData += $sqlDataTmp;
		}
		if ($sqlData) {
			$this->activeDao->baseUpdateData($id, $sqlData);
		}

		if ($editingMode == 'edit') {
			$this->success('保存成功', '', ['mode'=>'jumplink', 'okLink'=>url('actstar_manage/active/index')]);
		} else {
			$this->success('添加成功', '', ['mode'=>'jumplink', 'okLink'=>url('actstar_manage/active/index')]);
		}
	}

	private function doPictures($nowid, $flashatt) {
		$ids = array();
		$viewOrderId = 1;
		foreach ($flashatt as $key => $value) {
			$data = array(
				'vieworder' => $viewOrderId,
			);
			$this->activePictureDao->baseUpdateData($value, $data);
			$viewOrderId++;
			$ids[] = $key;
		}
		$data = array(
			'kid'	=> $nowid,
		);
		$this->activePictureDao->where('id', 'in', $ids)->update($data);
		//print_r($this->pictureDao->getLastSql());exit;
		$sqlData = array(
			'pic_num' => count($flashatt),
		);
		return $sqlData;
	}

	public function doDelete() {
		$id = input('param.id', '', '', 'intval');
		if (!$id) {
			$this->error('非法ID');
		}

		//获取信息(不解析)
		$info = $this->activeDao->get($id);

		//删除对应数据库信息
		$result = $this->activeDao->delInfo($id);
		if ($result !== false) {
			$this->success('删除成功', url('actstar_manage/active/index'));
		} else {
			$this->error('删除失败'.showDbError($this->activeDao));
		}
	}
	/**
	 * 清空抽奖信息线上：http://wx.ksmeishi.cn/actstar/activeDraw/doClear/kid/13.html
	 * 清空抽奖信息本地：http://www2.kszhuangxiu.com/actstar/activeDraw/doClear/kid/13.html
	 */
	public function doClearLottery() {
		$kid = input('param.kid', '', 'intval');

		$this->signupDao = model('Signup');
		$this->signupDao->where(['kid'=>$kid])->update(['lotteryprize_id'=>0]);

		$this->activeDao->baseUpdateData($kid, ['lottery_status'=>0]);

		$this->success('清空中奖信息成功');
	}
	
	/**
	 * ajax获取接口(列表)
	 */
	public function ajaxAssociate() {
		//获取活动列表
		$keyword = input('param.keyword', '', '', 'pwEscape');
		$map = [
			'title' => ['like', '%'.$keyword.'%']
		];
		list($list) = $this->activeDao->search($map, '');
		$newList = [];
		foreach ($list as $key => $value) {
			$tmp = [];
			$tmp['id'] = $value['id'];
			$tmp['text'] = $value['title'];
			$newList[] = $tmp;
		}
		$resultData = [
			'list' => $newList
		];
		$this->success('', '', $resultData);
	}
	
	/**
	 * ajax获取接口(信息)
	 */
	public function ajaxDataInfo() {
		$kid = input('param.id', '', '', 'intval');
		//获取活动信息
		$activeInfo = $this->activeDao->getInfo($kid);
		//获取活动图片列表
		list($pictureList) = $this->activePictureDao->getListByKid($kid);
		$info = array(
			'page_flag'	=> 'active_detail',
			'page_data'	=> $kid,
			'title'		=> $activeInfo['title'],
			'link'		=> config('system.site_share_domain').url('actstar/active/detail', ['kid'=>$kid]),
			'picList'	=> $pictureList,
		);
		$this->assign("info", $info);
		$html = $this->fetch('pagedestination:infopush_data_info');
		$this->success('', '', $html);
	}

}