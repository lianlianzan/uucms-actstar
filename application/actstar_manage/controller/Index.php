<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2018-07-11 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2020-12-28 13:18:00
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class Index extends ManageBase {

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '配置管理', '', 'index');
	}

	public function index() {
		//活动总数
		$activeDao = model('Active');
		$activeNum = $activeDao->countSearch([]);
		$this->assign('activeNum', $activeNum);
		
		//用户总数
		$userDao = model('User');
		$userNum = $userDao->countSearch([]);
		$this->assign('userNum', $userNum);
		
		//报名记录总数
		$signupDao = model('Signup');
		$signupNum = $signupDao->countSearch([]);
		$this->assign('signupNum', $signupNum);

		//报名记录总交易额
		$recordSumAmount = $signupDao->baseSumSearch(['pay_status'=>2]);
		$this->assign('recordSumAmount', $recordSumAmount);
		
		//获取微信公众号信息
		$weixinAccountDao = model('weixin_manage/WeixinAccount');
		$weixinAccountInfo = $weixinAccountDao->getInfo(config('system.authorize_wid'));
		$this->assign('authorize_domain', $weixinAccountInfo['authorize_domain']);

		//前台地址展示
		$prodUrl = 'http://'.$weixinAccountInfo['authorize_domain'].'/actstar/index/index/';
		$httpsUrl = 'https://'.$weixinAccountInfo['authorize_domain'].'/actstar/index/index/';
		$devUrl = config('system.site_host').'/actstar/index/index/';
		$this->assign('prodUrl', $prodUrl);
		$this->assign('httpsUrl', $httpsUrl);
		$this->assign('devUrl', $devUrl);
		
		//产品使用说明书
        $text = file_get_contents(APP_PATH.'/actstar_manage/README.md');
		require_once(EXTEND_PATH."HyperDown/Parser.php");
		$parser = new \HyperDown\Parser();
		$html = $parser->makeHtml($text);
		$this->assign('html', $html);
		
		return $this->fetch();
	}

}