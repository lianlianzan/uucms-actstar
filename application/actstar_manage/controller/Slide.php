<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2020-11-14 18:04:32
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 14:15:45
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class Slide extends ManageBase {

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '图片轮播管理', '', 'slide');

		$this->slideDao = model('Slide');
	}

	public function index() {
		$map = $parameter = array();

		$count = $this->slideDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($list) = $this->slideDao->search($map, $Page->getLimit());
		//print_r($list);exit;
		$this->assign('count', $count);
		$this->assign('pageShow', $pageShow);
		$this->assign('list', $list);

		return $this->fetch();
	}

	public function listorders() {
		$result = parent::listordersOrigin($this->slideDao);
		if ($result !== false) {
			$this->success('排序更新成功');
		} else {
			$this->error('排序更新失败');
		}
	}

	public function add() {
		$id = input('param.id', '', '', 'intval');

		if ($id) {
			$info = $this->slideDao->getInfo($id);
		} else {
			$info = [
				'status' => 1
			];
		}
		$this->assign('id', $id);
		$this->assign('info', $info);

		$this->assign('page_type_config', config('page_type_config')); //目标页面类型
		return $this->fetch();
	}

	public function doAdd() {
		$id = input('post.id', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');

		if ($id) {
			$info = $this->slideDao->get($id);
		}

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/slide', 'picurl', $info['picurl']);
		$uploadInfo && $data['picurl'] = $uploadInfo['saveFullPath'];

		//验证数据
		// $validate = validate('slide');
		// if (!$validate->check($data)) {
		//     $this->error($validate->getError());
		// }

		if ($id) {
			$result = $this->slideDao->baseUpdateData($id, $data);
			if ($result !== false) {
				$this->success('保存成功', url('actstar_manage/slide/index'));
			} else {
				$this->error('保存失败'.showDbError($this->slideDao));
			}
		} else {
			$data['create_time'] = $this->ts;
			$result = $this->slideDao->baseAddData($data);
			if ($result !== false) {
				$this->success('添加成功', url('actstar_manage/slide/index'));
			} else {
				$this->error('添加失败'.showDbError($this->slideDao));
			}
		}
	}

	public function doDelete() {
		$id = input('param.id', '', '', 'intval');

		//获取信息(不解析)
		$info = $this->slideDao->get($id);

		//删除对应数据库信息
		$result = $this->slideDao->delInfo($id);

		//删除图片文件
		$info['picurl'] && $this->delFtpFile($info['picurl']);

		if ($result !== false) {
			$this->success('删除成功', url('actstar_manage/slide/index'));
		} else {
			$this->error('删除失败'.showDbError($this->slideDao));
		}
	}

}