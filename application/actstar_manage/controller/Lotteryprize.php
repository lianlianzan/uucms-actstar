<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2017-09-27 16:40:00
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-02-03 22:06:05
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class Lotteryprize extends ManageBase {

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '奖品列表', '', 'lotteryprize');

		$this->prizeDao = model('Lotteryprize');

		$this->assign('prize_type_config', config('extend.prize_type_config')); //奖品类型
		$this->assign('prize_writeoff_config', config('extend.prize_writeoff_config')); //奖品核销
	}

	public function index() {
		$map = $parameter = array();
		
		$kid = input('param.kid', '', '', 'intval');
		if ($kid) {
			$map['kid'] = $kid;
			$parameter['kid'] = $kid;
		} else {
			$this->aceError('非法活动ID');
		}
		$this->assign('kid', $kid);

		$count = $this->prizeDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($list) = $this->prizeDao->search($map, $Page->getLimit());
		$this->assign('pageShow', $pageShow);
		$this->assign('list', $list);
		
		return $this->fetch();
	}

	public function listorders() {
		$vieworders = input('post.vieworders/a', '', '', 'pwEscape');
		foreach ($vieworders as $key => $value) {
			$data = array();
			$data['vieworder'] = $value;
			$this->prizeDao->where(array('id'=>$key))->update($data);
		}
		$this->success('排序设置成功', '');
	}

	public function add() {
		$id = input('param.id', '', '', 'intval');

		if ($id) {
			$info = $this->prizeDao->getInfo($id);
			$kid = $info['kid'];
		} else {
			$kid = input('param.kid', '', '', 'intval');
		}
		$this->assign('kid', $kid);
		$this->assign('id', $id);
		$this->assign('info', $info);

		return $this->fetch();
	}

	public function doAdd() {
		$id = input('post.id', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');

		if ($id) {
			$info = $this->prizeDao->get($id);
			$kid = $info['kid'];
		} else {
			$kid = input('post.kid', '', '', 'intval');
		}

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/lotteryprize', 'picurl', $info['picurl']);
		$uploadInfo && $data['picurl'] = $uploadInfo['saveFullPath'];

		if ($id) {
			$result = $this->prizeDao->baseUpdateData($id, $data);
			if ($result !== false) {
				$this->success('保存成功', url('actstar_manage/lotteryprize/index', ['kid'=>$kid]));
			} else {
				$this->error('保存失败'.showDbError($this->prizeDao));
			}
		} else {
			$data['kid'] = $kid;
			$result = $this->prizeDao->baseAddData($data);
			if ($result !== false) {
				$this->success('添加成功', url('actstar_manage/lotteryprize/index', ['kid'=>$kid]));
			} else {
				$this->error('添加失败'.showDbError($this->prizeDao));
			}
		}
	}

	public function doDelete() {
		$id = input('param.id', '', '', 'intval');
		if (!$id) {
			$this->error('非法ID');
		}

		//获取信息
		$info = $this->activeDao->get($id);

		//执行验证
		if ($info['manage_uid'] != $this->adminuid) {
			$this->error("非法删除操作，您没有权限");
		}

		//删除对应数据库信息
		$result = $this->prizeDao->delInfo($id);
		if ($result !== false) {
			$this->success('删除成功', url('actstar_manage/lotteryprize/index', ['kid'=>$kid]));
		} else {
			$this->error('删除失败'.showDbError($this->prizeDao));
		}
	}

}