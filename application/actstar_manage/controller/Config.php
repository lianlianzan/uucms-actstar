<?php
/**
 * @Author: Zhaojun(13040@qq.com)
 * @Date:   2019-01-10 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-18 10:37:12
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class Config extends ManageBase {

	private $config_space = 'global';

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '配置管理', '', 'config');

		$this->configDao = model('Config');
	}

	public function index() {
		$info = $this->configDao->getConfigsBySpace($this->config_space);
		$this->assign('info', $info);

		return $this->fetch();
	}

	public function doSet() {
		$data = input('post.data/a', '', '', 'pwEscape');

		$info = $this->configDao->getConfigsBySpace($this->config_space);

		//支付方式
		$allow_pay_method = input('post.allow_pay_method/a', '', '', 'pwEscape');
		$data['allow_pay_method'] = !empty($allow_pay_method) ? intval(array_sum($allow_pay_method)) : 0;
		
		//系统LOGO
		$uploadInfo = $this->comuploadFile('actstar/config', 'proj_logo', $activeInfo['proj_logo']);
		$uploadInfo && $data['proj_logo'] = $uploadInfo['saveFullPath'];

		//客服二维码
		$uploadInfo = $this->comuploadFile('actstar/config', 'kefu_ewm', $activeInfo['kefu_ewm']);
		$uploadInfo && $data['kefu_ewm'] = $uploadInfo['saveFullPath'];

		//分享图标
		$uploadInfo = $this->comuploadFile('actstar/config', 'share_icon', $activeInfo['share_icon']);
		$uploadInfo && $data['share_icon'] = $uploadInfo['saveFullPath'];

		foreach ($data as $key => $value) {
			$checkInfo = $this->configDao->getConfigByNameAndSpace($key, $this->config_space);
			$sqlData = [
				'config_value'  => $value,
			];
			if ($checkInfo) {
				$this->configDao->updateConfigByNameAndSpace($key, $this->config_space, $sqlData);
			} else {
				$sqlData['config_space'] = $this->config_space;
				$sqlData['config_name'] = $key;
				$sqlData['description'] = '';
				$this->configDao->insert($sqlData);
			}
		}

		$sys_identify = config('sys_identify');
		cache($sys_identify, NULL); //删除缓存数据

		$this->success("保存成功");
	}

}