<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2018-08-18 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-19 15:47:06
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class Article extends ManageBase {

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '文章管理', '', 'article');

		$this->articleDao = model('Article');
	}

	public function index() {
		//获取列表
		list($map, $parameter) = $this->getMap();

		$count = $this->articleDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($list) = $this->articleDao->search($map, $Page->getLimit());
		//print_r($list);exit;
		$this->assign('count', $count);
		$this->assign('pageShow', $pageShow);
		$this->assign('list', $list);

		//获取分类列表
		$categoryDao = model('ArticleCategory');
		list($categoryList) = $categoryDao->search('', '');
		$this->assign('categoryList', $categoryList);

		return $this->fetch();
	}

	private function getMap() {
		$map = $parameter = array();

		$title = input('param.title', '', '', 'pwEscape');
		if ($title) {
			$map['title'] = array('like', '%'.$title.'%');
			$parameter['title'] = $title;
		}
		$this->assign('title', $title);

		$content = input('param.content', '', '', 'pwEscape');
		if ($content) {
			$map['content'] = array('like', '%'.$content.'%');
			$parameter['content'] = $content;
		}
		$this->assign('content', $content);

		$this->assign('parameter', $parameter);
		return array($map, $parameter);
	}

	public function getDemoUrl() {
		$id = input('param.id', '', '', 'intval');
		$this->assign('id', $id);

		//获取文章信息
		$activeInfo = $this->articleDao->getInfo($id);
		$this->assign('activeInfo', $activeInfo);

		//获取微信公众号信息
		$weixinAccountDao = model('weixin_manage/WeixinAccount');
		$weixinAccountInfo = $weixinAccountDao->getInfo(config('system.authorize_wid'));
		$this->assign('authorize_domain', $weixinAccountInfo['authorize_domain']);

		//前台地址展示
		$prodUrl = 'http://'.$weixinAccountInfo['authorize_domain'].'/actstar/article/detail/arid/'.$id;
		$httpsUrl = 'https://'.$weixinAccountInfo['authorize_domain'].'/actstar/article/detail/arid/'.$id;
		$devUrl = config('system.site_host').'/actstar/article/detail/arid/'.$id;
		$this->assign('prodUrl', $prodUrl);
		$this->assign('httpsUrl', $httpsUrl);
		$this->assign('devUrl', $devUrl);

		echo $this->fetch()->getContent();
	}

	public function add() {
		$id = input('param.id', '', '', 'intval');

		if ($id) {
			$info = $this->articleDao->getInfo($id);
		} else {
			$info = [
				'status' => 1
			];
		}
		$this->assign('id', $id);
		$this->assign('info', $info);

		//获取分类列表
		$categoryDao = model('ArticleCategory');
		list($categoryList) = $categoryDao->search('', '');
		$this->assign('categoryList', $categoryList);

		return $this->fetch();
	}

	public function doAdd() {
		$id = input('post.id', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');

		if ($id) {
			$info = $this->articleDao->get($id);
		}

		//处理编辑器内容
		$data['content'] = ueditorContent($data['content']);

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/article', 'picurl', $info['picurl']);
		$uploadInfo && $data['picurl'] = $uploadInfo['saveFullPath'];

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/article', 'picurl2', $info['picurl2']);
		$uploadInfo && $data['picurl2'] = $uploadInfo['saveFullPath'];

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/article', 'picurl3', $info['picurl3']);
		$uploadInfo && $data['picurl3'] = $uploadInfo['saveFullPath'];

		//时间解析
		if (!$data['create_time']) {
			$data['create_time'] = $this->ts;
		} else {
			$data['create_time'] = str2time($data['create_time']);
		}

		//获取分类信息
		if ($data['cid']) {
			$categoryDao = model('ArticleCategory');
			$categoryInfo = $categoryDao->getInfo($data['cid']);
			$data['cate_name'] = $categoryInfo['name'];
		}

		//验证数据
		//$this->validateData($data, '');

		if ($id) {
			$result = $this->articleDao->baseUpdateData($id, $data);
			if ($result !== false) {
				$this->success('保存成功', url('actstar_manage/article/index'));
			} else {
				$this->error('保存失败'.showDbError($this->articleDao));
			}
		} else {
			$result = $this->articleDao->baseAddData($data);
			if ($result !== false) {
				$this->success('添加成功', url('actstar_manage/article/index'));
			} else {
				$this->error('添加失败'.showDbError($this->articleDao));
			}
		}
	}

	public function doDelete() {
		$id = input('param.id', '', '', 'intval');

		//获取信息(不解析)
		$articleInfo = $this->articleDao->get($id);

		$result = $this->articleDao->delInfo($id);

		//删除图片文件
		$articleInfo['picurl'] && $this->delFtpFile($articleInfo['picurl']);

		if ($result !== false) {
			$this->success('删除成功', url('actstar_manage/article/index'));
		} else {
			$this->error('删除失败'.showDbError($this->articleDao));
		}
	}
	
	/**
	 * ajax获取接口(列表)
	 */
	public function ajaxAssociate() {
		//获取文章列表
		$keyword = input('param.keyword', '', '', 'pwEscape');
		$map = [
			'title' => ['like', '%'.$keyword.'%']
		];
		list($list) = $this->articleDao->search($map, '');
		$newList = [];
		foreach ($list as $key => $value) {
			$tmp = [];
			$tmp['id'] = $value['id'];
			$tmp['text'] = $value['title'];
			$newList[] = $tmp;
		}
		$resultData = [
			'list' => $newList
		];
		$this->success('', '', $resultData);
	}

	/**
	 * ajax获取接口(信息)
	 */
	public function ajaxDataInfo() {
		$arid = input('param.id', '', '', 'intval');
		//获取文章信息
		$articleInfo = $this->articleDao->getInfo($arid);
		$info = array(
			'page_flag'	=> 'article_detail',
			'page_data'	=> $arid,
			'title'		=> $articleInfo['title'],
			'link'		=> config('system.site_share_domain').url('actstar/article/detail', ['arid'=>$arid]),
		);
		$this->assign("info", $info);
		$html = $this->fetch('pagedestination:infopush_data_info');
		$this->success('', '', $html);
	}

}