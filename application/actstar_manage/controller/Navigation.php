<?php
/**
 * @Author: lianlianzan(13040@qq.com)
 * @Date:   2019-01-10 16:40:16
 * @Last Modified by:   lianlianzan
 * @Last Modified time: 2021-03-17 15:57:29
 */

namespace app\actstar_manage\controller;
use app\common\controller\ManageBase;

class Navigation extends ManageBase {

	function initialize() {
		parent::initialize();
		$this->saveAndGetConfigIdentify(model('Config'), '导航菜单', '', 'navigation');

		$this->navigationDao = model('Navigation');

		$this->assign('page_type_config', config('page_type_config')); //目标页面类型
	}

	public function index() {
		list($map, $parameter) = $this->getMap();

		$count = $this->navigationDao->countSearch($map);
		$Page = new \org\util\PageBootstrap($count, config('PER_PAGE'), $parameter);
		$pageShow = $Page->show();
		list($list) = $this->navigationDao->search($map, $Page->getLimit(), $orderby);
		$this->assign("count", $count);
		$this->assign('pageShow', $pageShow);
		$this->assign("list", $list);

		return $this->fetch();
	}

	private function getMap() {
		$map = $parameter = array();

		$status = input('param.status', '', '', 'pwEscape');
		if ($status) {
			$map['status'] = $status;
			$parameter['status'] = $status;
		}
		$this->assign('status', $status);

		$name = input('param.name', '', '', 'pwEscape');
		if ($name) {
			$map['name'] = array('like', '%'.$name.'%');
			$parameter['name'] = $name;
		}
		$this->assign('name', $name);

		return array($map, $parameter);
	}

	public function listorders() {
		$result = parent::listordersOrigin($this->navigationDao);
		if ($result !== false) {
			$this->success('排序更新成功');
		} else {
			$this->error('排序更新失败'.showDbError($this->navigationDao));
		}
	}

	public function add() {
		$id = input('param.id', '', '', 'intval');

		if ($id) {
			$info = $this->navigationDao->getInfo($id);
		} else {
			$info = [
				'status'	=> 1
			];
		}
		$this->assign('id', $id);
		$this->assign('info', $info);

		return $this->fetch();
	}

	public function doAdd() {
		$id = input('post.id', '', '', 'intval');
		$data = input('post.data/a', '', '', 'pwEscape');

		if ($id) {
			$info = $this->navigationDao->get($id);
		}

		//普通上传图片
		$uploadInfo = $this->comuploadFile('actstar/navigation', 'picurl', $info['picurl']);
		$uploadInfo && $data['picurl'] = $uploadInfo['saveFullPath'];

		if ($id) {
			$result = $this->navigationDao->baseUpdateData($id, $data);
			if ($result === false) {
				$this->error('保存失败'.showDbError($this->navigationDao));
			}
		} else {
			$data['status'] = 1;
			$data['create_time'] = $this->ts;
			$result = $id = $this->navigationDao->baseAddData($data);
			if ($result === false) {
				$this->error('添加失败'.showDbError($this->navigationDao));
			}
		}

		if ($id) {
			$this->success('保存成功', url('actstar_manage/navigation/index'));
		} else {
			$this->success('添加成功', url('actstar_manage/navigation/index'));
		}
	}

	public function doDelete() {
		$id = input('param.id', '', '', 'intval');

		$result = $this->navigationDao->delInfo($id);
		if ($result !== false) {
			$this->success('删除成功', url('actstar_manage/navigation/index'));
		} else {
			$this->error('删除失败'.showDbError($this->navigationDao));
		}
	}

}